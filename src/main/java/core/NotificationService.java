package core;

import core.event.subscriber.ChangePosition;
import models.Notification;
import models.PlayerStatistic;
import util.Utils;

import java.util.List;
import java.util.function.Predicate;

public class NotificationService {

    private LeaderBoard board;

    private ChangePosition changePositionNotificationAction = event -> {
        if (board == null) System.out.println("Can`t send notification board is null");
        List<PlayerStatistic> boardSnapshot = board.getBoardSnapshot();
        Predicate<PlayerStatistic> finderById = playerStatistic -> playerStatistic.getPlayerId() == event.getUserId();
        List<PlayerStatistic> nearPlayers = Utils.getNeighborsInList(boardSnapshot, 5, finderById);

        Notification notification = new Notification();
        notification.setTotalPlayers(boardSnapshot.size());
        notification.setClosestFivePlayers(nearPlayers);
        notification.setUserId(event.getUserId());
        notification.setOldPosition(event.getOldPosition());
        notification.setNewPosition(event.getNewPosition());

        System.out.println(notification);
    };

    public NotificationService(LeaderBoard board) {
        this.board = board;
    }

    public void enable() {
        this.board.registerChangePositionEvent(changePositionNotificationAction);
    }

    public void disable() {
        this.board.removeChangePositionEvent(changePositionNotificationAction);
    }

}
