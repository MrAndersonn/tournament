package core.event;

public class ChangePositionEvent {
    private long userId;
    private long oldPosition;
    private long newPosition;

    public static ChangePositionEvent of(long userId, long oldPosition, long newPosition) {
        ChangePositionEvent event = new ChangePositionEvent();
        event.userId = userId;
        event.newPosition = newPosition;
        event.oldPosition = oldPosition;
        return event;
    }

    public long getUserId() {
        return userId;
    }

    public long getOldPosition() {
        return oldPosition;
    }

    public long getNewPosition() {
        return newPosition;
    }

    @Override
    public String toString() {
        return "ChangePositionEvent{" +
                "userId=" + userId +
                ", oldPosition=" + oldPosition +
                ", newPosition=" + newPosition +
                '}';
    }
}