package core.event.subscriber;

import core.event.ChangePositionEvent;

@FunctionalInterface
public interface ChangePosition {
    void onChange(ChangePositionEvent event);
}
