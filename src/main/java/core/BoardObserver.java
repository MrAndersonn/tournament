package core;


import core.event.ChangePositionEvent;
import core.event.subscriber.ChangePosition;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class BoardObserver {

    private List<ChangePosition> changePortionSubscribers = new ArrayList<>();

    public void removeSubscriber(ChangePosition subscriber) {
        changePortionSubscribers.remove(subscriber);
    }

    public void registerSubscriber(ChangePosition subscriber) {
        changePortionSubscribers.add(subscriber);
    }

    public void invoke(ChangePositionEvent event) {
        changePortionSubscribers.forEach(handler -> handler.onChange(event));
    }
}