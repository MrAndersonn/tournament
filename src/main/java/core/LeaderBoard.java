package core;

import core.event.ChangePositionEvent;
import core.event.subscriber.ChangePosition;
import models.Player;
import models.PlayerStatistic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class LeaderBoard {

    private static final Lock BOARD_LOCK = new ReentrantLock();

    private final BoardObserver OBSERVER = new BoardObserver();
    private List<PlayerStatistic> leaderBoard = Collections.synchronizedList(new LinkedList<>());

    public void registerChangePositionEvent(ChangePosition subscriber) {
        OBSERVER.registerSubscriber(subscriber);
    }

    public void removeChangePositionEvent(ChangePosition subscriber) {
        OBSERVER.removeSubscriber(subscriber);
    }

    public void updatePlayerScore(Long userId, Long scoreDelta) {
        BOARD_LOCK.lock();
        leaderBoard.stream()
                .filter(playerStatistic -> playerStatistic.getPlayerId() == userId)
                .findFirst()
                .ifPresent(playerStatistic -> {
                    updatePlayerOnBoard(playerStatistic, scoreDelta);
                    updateBoard();
                });
        BOARD_LOCK.unlock();
    }

    public void registerPlayer(Player player) {
        int position = getPosition(player);
        BOARD_LOCK.lock();
        leaderBoard.add(position, new PlayerStatistic(player));
        updateBoard();
        BOARD_LOCK.unlock();
    }

    public List<PlayerStatistic> getBoardSnapshot() {
        BOARD_LOCK.lock();
        List<PlayerStatistic> snapshot = leaderBoard.stream()
                .map(playerStatistic -> new PlayerStatistic(playerStatistic.getPlayer(), playerStatistic.getPosition()))
                .collect(Collectors.toList());
        BOARD_LOCK.unlock();

        return snapshot;
    }

    private void updateBoard() {
        List<ChangePositionEvent> events = new ArrayList<>(leaderBoard.size());
        for (int position = 0; position < leaderBoard.size(); position++) {
            PlayerStatistic playerStatistic = leaderBoard.get(position);
            int oldPosition = playerStatistic.getPosition();
            if (oldPosition != position) {
                ChangePositionEvent changePositionEvent = ChangePositionEvent.of(playerStatistic.getPlayerId(), oldPosition, position);
                playerStatistic.setPosition(position);
                events.add(changePositionEvent);
            }
        }
        events.forEach(OBSERVER::invoke);
    }

    private void updatePlayerOnBoard(PlayerStatistic playerStatistic, Long scoreDelta) {
        leaderBoard.remove(playerStatistic);
        playerStatistic.updateScore(scoreDelta);
        int position = getPosition(playerStatistic.getPlayer());
        leaderBoard.add(position, playerStatistic);
    }

    private int getPosition(Player player) {
        int position = -1;
        for (int i = 0; i < leaderBoard.size(); i++) {
            if (leaderBoard.get(i).getScore() < player.getScore()) {
                position = i;
                break;
            }
        }

        if (position == -1) {
            position = leaderBoard.size();
        }
        return position;
    }
}
