package util;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class Utils {

    public static <T> List<T> getNeighborsInList(List<T> list, int neighborsCount, Predicate<T> rootObjectFinder) {
        int leftIndex = -1;
        int rightIndex = -1;

        for (int i = 0; i < list.size(); i++) {
            T playerStatistic = list.get(i);
            if (rootObjectFinder.test(playerStatistic)) {
                leftIndex = i;
                rightIndex = i;
                for (int j = 0; j < neighborsCount; j++) {
                    if (rightIndex - leftIndex == neighborsCount - 1) break;

                    if (leftIndex - 1 >= 0) {
                        leftIndex--;
                    }

                    /*for even numbers*/
                    if (rightIndex - leftIndex == neighborsCount - 1) break;

                    if (rightIndex + 1 < list.size()) {
                        rightIndex++;
                    }
                }
                break;
            }
        }

        if (leftIndex == -1 || rightIndex == -1) return Collections.emptyList();

        return list.subList(leftIndex, rightIndex + 1);
    }

    public static long randomInRange(long min, long max) {
        return min + (long) (Math.random() * (max - min));
    }
}
