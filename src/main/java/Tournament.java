import core.LeaderBoard;
import core.NotificationService;
import models.Player;
import util.Utils;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Tournament {

    private static final int SECOND = 1000;
    private static AtomicInteger USER_ID_SEQUENCE = new AtomicInteger(1);
    private LeaderBoard board = new LeaderBoard();

    public static void main(String[] args) {
        Tournament simulatedTournament = new Tournament();
        NotificationService service = new NotificationService(simulatedTournament.board);
        service.enable();

        getSimulatePlayersWorker(simulatedTournament.board).start();
        getUpdatingScoreWorker(simulatedTournament.board).start();
    }

    private static Thread getSimulatePlayersWorker(LeaderBoard board) {
        return new Thread(() -> {
            while (true) {
                IntStream
                        .range(0, (int) (Math.random() * 5))
                        .forEach(value -> {
                            Player player = new Player(USER_ID_SEQUENCE.getAndIncrement(), UUID.randomUUID().toString());
                            board.registerPlayer(player);
                        });
                try {
                    Thread.sleep(Utils.randomInRange(SECOND, 10 * SECOND));
                } catch (Exception e) {/*ignore*/}
            }
        });
    }

    private static Thread getUpdatingScoreWorker(LeaderBoard board) {
        return new Thread(() -> {
            while (true) {
                long randomUserId = (long) (Math.random() * USER_ID_SEQUENCE.get());
                board.updatePlayerScore(randomUserId, Utils.randomInRange(1, 10));
                try {
                    Thread.sleep(Utils.randomInRange(SECOND, 4 * SECOND));
                } catch (Exception e) {/*ignore*/}
            }
        });
    }
}
