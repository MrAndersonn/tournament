package models;

public class PlayerStatistic {
    private Player player;
    private int position;

    public PlayerStatistic(Player player, int position) {
        this.player = player;
        this.position = position;
    }

    public PlayerStatistic(Player player) {
        this.player = player;
        this.position = -1;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Player getPlayer() {
        return player;
    }

    public long getScore() {
        return player.getScore();
    }

    public void updateScore(long scoreDelta) {
        player.setScore(player.getScore() + scoreDelta);
    }

    public long getPlayerId() {
        return player.getId();
    }

    @Override
    public String toString() {
        return "{" +
                "player=" + player.getId() +
                ", score=" + player.getScore() +
                ", position=" + position +
                '}';
    }
}