package models;

import util.Utils;

public class Player {
    private long id;
    private String name;
    private long score;

    public Player(long id, String name) {
        this.id = id;
        this.name = name;
        this.score = Utils.randomInRange(100, 350);
    }

    public long getId() {
        return id;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
