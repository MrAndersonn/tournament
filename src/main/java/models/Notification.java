package models;

import java.util.List;
import java.util.stream.Collectors;

public class Notification {

    private static final String UP_ARROW = "\u2191";
    private static final String DOWN_ARROW = "\u2193";
    private List<PlayerStatistic> closestFivePlayers;
    private long totalPlayers;
    private long userId;
    private long oldPosition;
    private long newPosition;

    public void setClosestFivePlayers(List<PlayerStatistic> closestFivePlayers) {
        this.closestFivePlayers = closestFivePlayers;
    }

    public void setTotalPlayers(long totalPlayers) {
        this.totalPlayers = totalPlayers;
    }


    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setOldPosition(long oldPosition) {
        this.oldPosition = oldPosition;
    }

    public void setNewPosition(long newPosition) {
        this.newPosition = newPosition;
    }

    @Override
    public String toString() {
        String header =
                "\n--------------------------------------------------" + "\n" +
                        "|" + "\t\t" + "  Notification  " + "\n" +
                        "|" + "\t\t  userId:" + userId + "\n" +
                        "|" + "\t\t  totalPlayers: " + totalPlayers + "\n" +
                        "--------------------------------------------------" + "\n";

        String body = closestFivePlayers.stream()
                .map(playerStatistic -> {
                    String positionInfo = "|\t\t" + playerStatistic.getPosition() + ". id: " + playerStatistic.getPlayerId() + ", score: " + playerStatistic.getScore();
                    if (playerStatistic.getPlayerId() == userId) {
                        String positionChange = "";
                        if (oldPosition != -1) {
                            long delta = oldPosition - newPosition;
                            if (delta > 0) positionChange = UP_ARROW;
                            else if (delta < 0) {
                                positionChange = DOWN_ARROW;
                                delta = delta * -1;
                            }
                            positionChange += delta;
                        }
                        positionInfo += " " + positionChange + " <-------- your id";
                    }
                    return positionInfo;
                })
                .collect(Collectors.joining("\n"));


        String footer = "\n--------------------------------------------------\n";


        return header + body + footer;
    }
}
